
public class Persona {
	private String nombre;
	private String apellidos;
	private int id;
	
	Persona (String nombre, String apellidos, int id){
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.id = id;
	}
	public void getDatosPersonales() {
		String completo = this.nombre + " " + this.apellidos;
		System.out.println("Nombre: " + completo);
		System.out.println("Id: " + id);
	}
}
