
public class Profesor extends Empleado {
	String depto;
	Profesor(String nombre, String apellidos, int id, int ingreso, int anexo, String depto) {
		super(nombre, apellidos, id, ingreso, anexo);
		this.depto = depto;
	}
	public void getDatosProfesor() {
		this.getDatosEmpleado();
		System.out.println("Departamento: " + this.depto);
	}
}
