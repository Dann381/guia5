
public class Administrativo extends Empleado{
		String seccion;
	Administrativo(String nombre, String apellidos, int id, int ingreso, int anexo, String seccion) {
		super(nombre, apellidos, id, ingreso, anexo);
		this.seccion = seccion;
	}
	public void getDatosAdministrativo() {
		this.getDatosEmpleado();
		System.out.println("Departamento: " + this.seccion);
	}
}
