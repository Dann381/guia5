
public class Empleado extends Persona{
	int ingreso;
	int anexo;
	Empleado(String nombre, String apellidos, int id, int ingreso, int anexo) {
		super(nombre, apellidos, id);
		this.ingreso = ingreso;
		this.anexo = anexo;
	}
	public void getDatosEmpleado() {
		this.getDatosPersonales();
		System.out.println("Año de ingreso: " + this.ingreso);
		System.out.println("Número de anexo: " + this.anexo);
	}
}
