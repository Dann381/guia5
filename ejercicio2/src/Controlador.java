import java.util.ArrayList;

public class Controlador {
	
	Controlador (){
		ArrayList<String> cursos = new ArrayList<String>();
		cursos.add("Programación Avanzada");
		cursos.add("Cálculo II");
		cursos.add("Procesos Metabólicos Celulares");
		cursos.add("Física General");
		cursos.add("Autogestión del Aprendizaje");
		cursos.add("Probabilidad y Estadística");
		
		
		Persona persona1 = new Persona("Daniel","Tobar Hormazábal", 2020430004);
		persona1.getDatosPersonales();
		
		System.out.println("\n");
		
		Estudiante estudiante1 = new Estudiante("Daniel", "Tobar Hormazábal", 2020430004, cursos);
		estudiante1.getDatosEstudiante();
		
		System.out.println("\n");
		
		Empleado empleado1 = new Empleado("Daniel", "Tobar Hormazábal", 2020430004, 2021, 711234);
		empleado1.getDatosEmpleado();
		
		System.out.println("\n");
		
		Profesor profesor1 = new Profesor("Fabio", "Durán Verdugo", 123456789, 2015, 711235, "Bioinformática");
		profesor1.getDatosProfesor();
		
		System.out.println("\n");
		
		Administrativo administrativo1 = new Administrativo("Juanita", "Pérez Pereira", 123456788, 2015, 711236, "Secretaría");
		administrativo1.getDatosAdministrativo();
		//System.out.println("\n");
	}
}
