import java.time.LocalDate;

public class Objeto {
	private String titulo;
	private int codigo;
	private LocalDate publicacion;

	Objeto (String titulo, int codigo, LocalDate publicacion){
		this.titulo = titulo;
		this.codigo = codigo;
		this.publicacion = publicacion;
	}
	public String getTitulo() {
		return titulo;
	}
	public int getCodigo() {
		return codigo;
	}
	public LocalDate getPublicacion() {
		return publicacion;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public void setPublicacion(LocalDate publicacion) {
		this.publicacion = publicacion;
	}
}

