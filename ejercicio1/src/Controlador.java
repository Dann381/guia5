import java.time.LocalDate;
import java.util.Scanner;

public class Controlador {
	
	Controlador (){
		//Escaner
		Scanner input = new Scanner(System.in);
		
		
		//Fechas cualquiera
		LocalDate fecha = LocalDate.of(1897, 5, 26);
		LocalDate fecha1 = LocalDate.of(2019, 7, 2);
		LocalDate hoy = LocalDate.now();
		LocalDate anteayer = hoy.minusDays(2);
		
		
		//Creación objeto tipo libro
		Libro libro = new Libro("Drácula", 0, fecha);
		libro.Prestar(anteayer);
		libro.muestra_detalle();
		System.out.print("\n");
		
		
		//Creación objeto tipo revista
		Revista revista = new Revista("One Punch Man: La voluntad del discípulo", 1, fecha1, 1, "Cómic");
		revista.Prestar(anteayer);
		revista.muestra_detalle();
		System.out.println("Prestado el día: " + revista.getFechaPresta());
		System.out.println("Días que ha estado prestado: " + revista.getTiempo());
		System.out.println("La revista caduca en: " + revista.getCaducidad());
		System.out.print("\n");
		System.out.println("Estado de pérdida: " + revista.getPerdido());		
		}		
}
