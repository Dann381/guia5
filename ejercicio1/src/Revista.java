import java.time.LocalDate;

public class Revista extends Libro {
	
	private int num_edicion;
	private String genero = "";
	private LocalDate caducidad;
	private Boolean perdida;

	Revista(String titulo, int codigo, LocalDate publicacion, int num_edicion, String genero) {
		super(titulo, codigo, publicacion);
		this.num_edicion = num_edicion;
		this.genero = genero;
		LocalDate hoy = LocalDate.now();
		LocalDate dia = hoy.plusDays(5);
		this.caducidad = dia;
		this.perdida = false;
	}
	public int getNum_edicion() {
		return num_edicion;
	}
	public void setNum_edicion(int num_edicion) {
		this.num_edicion = num_edicion;
	}
	public String getGenero() {
		return this.genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	
	public String getPerdido() {
		String respuesta = "";
		int limite = 0;
		if (this.genero == "Cómic") {
			limite = 1;
		}
		else if (this.genero == "Científica" || this.genero == "Deportiva") {
			limite = 2;
		}
		else if (this.genero == "Gastronómica") {
			limite = 3;
		}
		int tiempo = super.getTiempo();
		if (tiempo > limite) {
			this.perdida = true;
		}
		else if (tiempo < limite) {
			this.perdida = false;
		}
		if (this.perdida == false) {
			respuesta = "Perdida";
		}
		else if (this.perdida == true) {
			respuesta = "No perdida";
		}
		return respuesta;
	}
	public LocalDate getCaducidad() {
		return this.caducidad;
	}
	public void muestra_detalle() {
		System.out.println("Libro: " + super.getTitulo());
		System.out.println("Año de publicación: " + super.getPublicacion());
		
		System.out.println("Código: " + super.getCodigo());
		System.out.println("Número de edición: " + this.getNum_edicion());
		System.out.println("Género: " + this.getGenero());
		if (this.getPrestado() == false) {
			System.out.println("Estado: Sin prestar");
		}
		else if (this.getPrestado() == true) {
			System.out.println("Estado: Prestado");
		}
		
	}	
}
