import java.time.LocalDate;
import static java.time.temporal.ChronoUnit.DAYS;

public class Libro extends Objeto{
	
	private Boolean prestado = false;
	private LocalDate fechapresta;
	
	Libro(String titulo, int codigo, LocalDate publicacion) {
		super(titulo, codigo, publicacion);

	}
	public Boolean getPrestado() {
		return prestado;
	}
	public void setPrestado(Boolean prestado) {
		this.prestado = prestado;
	}
	public void Prestar(LocalDate fechapresta) {
		this.fechapresta = fechapresta;
		this.prestado = true;
	}
	public LocalDate getFechaPresta() {
		return this.fechapresta;
	}
	public int getTiempo() {
		int tiempo = 0;
		if (this.prestado = false) {
			System.out.println("Este objeto aún no ha sido prestado");
		}
		else if(this.prestado = true){
			LocalDate ahora = LocalDate.now();
			tiempo = (int)(DAYS.between(this.fechapresta, ahora));
		}
		return tiempo;
	}
	public int getCosto() {
		int costo = 0;
		if (this.prestado = false) {
			System.out.println("Este libro no ha sido prestado");
		}
		else if (this.prestado = true) {
			int tiempo = this.getTiempo();
			if (tiempo <= 5) {
				costo = 0;
			}
			else if(tiempo > 5){
				costo = (tiempo - 5)*1290;
			}		
		}
		return costo;
	}
	public void muestra_detalle() {
		System.out.println("Libro: " + super.getTitulo());
		System.out.println("Año de publicación: " + super.getPublicacion());
		System.out.println("Código: " + super.getCodigo());
		if (prestado == false) {
			System.out.println("Estado: Sin prestar");
		}
		else if (prestado == true) {
			System.out.println("Estado: Prestado");
		}
	}
	
	
	
	

}
